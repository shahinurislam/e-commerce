<?php
include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Product\Product;
use App\Cart\Cart;

Session::init();
$productObj  = new Product();
$cartObj     = new Cart();
$allProducts = $productObj->getAllProducts();
$cartData    = $cartObj->getCartAll();
$subtotal    = $cartObj->getTotalPrice();

include_once 'include/header.php';
?>


<div class="container ">
    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        <?php
            if( (count($cartData)>0) && ($cartData != false) ) {
            foreach ($cartData as $cartProduct){
        ?>
        <tr>
            <td data-th="Product">
                <div class="row">
                    <div class="col-sm-2 hidden-xs"><img src="<?php echo 'view/'.$cartProduct['image'] ?>" alt="..." class="img-responsive"/></div>
                    <div class="col-sm-10">
                        <h4 class="nomargin"><?php echo $cartProduct['name'] ?></h4>
                        <p><?php echo $cartProduct['short_description'] ?></p>
                    </div>
                </div>
            </td>
            <td data-th="Price"><?php echo '$'.$cartProduct['price'] ?></td>
            <td data-th="Quantity">
                <input type="number" class="form-control text-center" value="<?php echo '$'.$cartProduct['qti']; ?>">
            </td>
            <td data-th="Subtotal" class="text-center"><?php
                echo '$'.$cartProduct['price'] * $cartProduct['qti'] ;

                ?></td>
            <td class="actions" data-th="">
                <button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
                <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
            </td>
        </tr>
        <?php
                }
            }
        ?>

        </tbody>
        <tfoot style="
    border-top: 1px solid #ddd;
">
        <tr>
            <td>   </td>
            <td>   </td>
            <td>   </td>
            <td><h5>Subtotal<br>  <hr />Estimated shipping</h5> <hr /><h3>Total</h3>  <hr /></td>
            <td class="text-right"><h5><strong><?php echo '$'.$subtotal; ?><br>  <hr />$6.94</strong></h5>  <hr /><h3><?php echo '$'.$subtotal; ?></h3>  <hr /></td>
        </tr>
        <tr class="visible-xs">
            <td class="text-center"><strong>Total 1.9</strong></td>
        </tr>
        <tr>
            <td><a href="#" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong></strong></td>
            <td><a href="view/front/payment.php" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
        </tr>
        </tfoot>
    </table>
</div>


<?php
include_once 'include/footer.php';
?>
