<?php

include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;


Session::init();

?>

<?php include_once('include/header.php'); ?>


<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <div class="well" style="padding-left: 35px;">
                    <img id="img1" src="sweet1.jpg" alt="" height="100" width="100"/>
                </div>
                <br/>
                <br/>
                <div class="panel">
                    <div class="panel-body">
                        <h4><b>Description</b></h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                    </div>
                </div>
                <h4>Contact</h4>
                <hr/>
                <div class="divThree">
                    <a href="">
                        <span class="glyphicon glyphicon-earphone"></span>
                        +8800000
                    </a>
                    <hr/>
                </div>
                <hr/>
            </div>
            <div class="col-lg-5">
                <div class="panel panel-success divOne">
                    <div class="panel-body">
                        <h4>Category</h4>
                        <h4>Price</h4>
                    </div>
                </div>

                <div class="divOne">
                    <div class="col-lg-6">
                        <p>Code</p>
                        <hr/>
                        <p>Name</p>
                        <hr/>
                        <p>Size</p>
                        <hr/>
                        <p>Ad Created</p>
                        <hr/>
                        <p>Last Update</p>
                        <hr/>
                    </div>
                    <div class="col-lg-6">
                        <p>001</p>
                        <hr/>
                        <p>Shirt</p>
                        <hr/>
                        <p>Large</p>
                        <hr/>
                        <p>1/1/1</p>
                        <hr/>
                        <p>1/1/1</p>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section>









<?php include_once('include/footer.php'); ?>
