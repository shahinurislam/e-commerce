<?php

include_once('../../vendor/autoload.php');
use App\Session;


Session::init();

if(Session::checkSession() == false) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $user = new App\user\User();
        $email = $_POST['email'];
        $pass = md5($_POST['password']);
        $result = $user->getUserByNameOrEmail($email, $pass);
        if ($result != null) {
            $data = array();
            $data['id'] = $result->id;
            $data['name'] = $result->name;
            $data['email'] = $result->email;
            $data['rule'] = $result->rule;

            App\Auth::login($data);
            if (Session::checkSession() == true) {

                $_SESSION['id'] = $data['id'];
                            $userId = $_SESSION['id'];
                        $users = new App\user\User();
                       $userId2 = $users->userId($userId);
                header('location: view/front/includs/header.php?id='.$userId2.'');

            } else {
                header("Location:".App\Helper::config('config.basePath'));
            }
        }
    }else{
        header("Location: ".App\Helper::config('config.basePath'));
    }
}else{
    header("Location: ".App\Helper::config('config.basePath'));
}