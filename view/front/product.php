<?php

include_once('../../vendor/autoload.php');
use App\Session;
use App\Auth;
use App\Product\Product;
use App\Cart\Cart;

Session::init();
$productObj  = new Product();
$cartObj     = new Cart();
$allProducts = $productObj->getAllProducts();
$cartData    = $cartObj->getCartAll();
$subtotal    = $cartObj->getTotalPrice();

//echo "<pre>";
//print_r( $CartObj->getCartAll() );
//echo "</pre>";

?>
<?php include_once('include/header.php'); ?>
<div class="banner banner1">
    <div class="container">
        <h2>Great Offers on <span>Mobiles</span> Flat <i>35% Discount</i></h2>
    </div>
</div>

<div class="breadcrumb_dress">
    <div class="container">
        <ul>
            <li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a> <i>/</i></li>
            <li>Products</li>
        </ul>
    </div>
</div>

<div class="mobiles">
    <div class="container">
        <div class="w3ls_mobiles_grids">
            <div class="col-md-4 w3ls_mobiles_grid_left">
                <div class="w3ls_mobiles_grid_left_grid">
                    <h3>Categories</h3>
                    <div class="w3ls_mobiles_grid_left_grid_sub">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title asd">
                                        <a class="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>New Arrivals
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body panel_text">
                                        <ul>
                                            <li><a href="products.html">Mobiles</a></li>
                                            <li><a href="products1.html">Laptop</a></li>
                                            <li><a href="products2.html">Tv</a></li>
                                            <li><a href="products.html">Wearables</a></li>
                                            <li><a href="products2.html">Refrigerator</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>Accessories
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body panel_text">
                                        <ul>
                                            <li><a href="products2.html">Grinder</a></li>
                                            <li><a href="products2.html">Heater</a></li>
                                            <li><a href="products2.html">Kid's Toys</a></li>
                                            <li><a href="products2.html">Filters</a></li>
                                            <li><a href="products2.html">AC</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="panel_bottom">
                            <li><a href="products.html">Summer Store</a></li>
                            <li><a href="products.html">Featured Brands</a></li>
                            <li><a href="products.html">Today's Deals</a></li>
                            <li><a href="products.html">Latest Watches</a></li>
                        </ul>
                    </div>
                </div>
                <div class="w3ls_mobiles_grid_left_grid">
                    <h3>Color</h3>
                    <div class="w3ls_mobiles_grid_left_grid_sub">
                        <div class="ecommerce_color">
                            <ul>
                                <li><a href="#"><i></i> Red(5)</a></li>
                                <li><a href="#"><i></i> Brown(2)</a></li>
                                <li><a href="#"><i></i> Yellow(3)</a></li>
                                <li><a href="#"><i></i> Violet(6)</a></li>
                                <li><a href="#"><i></i> Orange(2)</a></li>
                                <li><a href="#"><i></i> Blue(1)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="w3ls_mobiles_grid_left_grid">
                    <h3>Price</h3>
                    <div class="w3ls_mobiles_grid_left_grid_sub">
                        <div class="ecommerce_color ecommerce_size">
                            <ul>
                                <li><a href="#">Below $ 100</a></li>
                                <li><a href="#">$ 100-500</a></li>
                                <li><a href="#">$ 1k-10k</a></li>
                                <li><a href="#">$ 10k-20k</a></li>
                                <li><a href="#">$ Above 20k</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 w3ls_mobiles_grid_right">
                <div class="col-md-6 w3ls_mobiles_grid_right_left">
                    <div class="w3ls_mobiles_grid_right_grid1">
                        <img src="images/46.jpg" alt=" " class="img-responsive">
                        <div class="w3ls_mobiles_grid_right_grid1_pos1">
                            <h3>Smart Phones<span>Up To</span> 15% Discount</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 w3ls_mobiles_grid_right_left">
                    <div class="w3ls_mobiles_grid_right_grid1">
                        <img src="images/47.jpg" alt=" " class="img-responsive">
                        <div class="w3ls_mobiles_grid_right_grid1_pos">
                            <h3>Top 10 Latest<span>Mobile </span>&amp; Accessories</h3>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>

                <div class="w3ls_mobiles_grid_right_grid2">
                    <div class="w3ls_mobiles_grid_right_grid2_left">
                        <h3>Showing Results: 0-1</h3>
                    </div>
                    <div class="w3ls_mobiles_grid_right_grid2_right">
                        <select name="select_item" class="select_item">
                            <option selected="selected">Default sorting</option>
                            <option>Sort by popularity</option>
                            <option>Sort by average rating</option>
                            <option>Sort by newness</option>
                            <option>Sort by price: low to high</option>
                            <option>Sort by price: high to low</option>
                        </select>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="w3ls_mobiles_grid_right_grid3">

                    <?php
                        if( ($allProducts != null) && (count($allProducts)>0) ){
                            foreach ($allProducts as $product){
                    ?>
                    <div class="col-md-4 agileinfo_new_products_grid agileinfo_new_products_grid_mobiles">
                        <div class="agile_ecommerce_tab_left mobiles_grid">
                            <div class="hs-wrapper hs-wrapper2">
                                <?php
                                $image          = json_decode( explode('; ', $product['images'] )[0] );
                                    echo '<img style="animation:0s ease 0s normal none 1 running none" src="view/'.$image->productImg.'" alt=" " class="img-responsive">';
                                ?>
                                <div class="w3_hs_bottom w3_hs_bottom_sub1">
                                    <ul>
                                        <li>
                                            <a href="#" data-toggle="modal" data-target="#myModal9"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h5><a href="single.html"><?php echo $product['name']; ?></a></h5>
                            <div class="simpleCart_shelfItem">
                                <p><i class="item_price">$<?php echo $product['price']; ?></i></p>
                                <button type="submit" class="w3ls-cart add-product-cart" data-id="<?php echo $product['id']; ?>">Add to cart</button>
                            </div>
                            <div class="mobiles_grid_pos">
                                <h6>New</h6>
                            </div>
                        </div>
                    </div>
                    <?php
                            }
                        }
                    ?>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="newsletter">
    <div class="container">
        <div class="col-md-6 w3agile_newsletter_left">
            <h3>Newsletter</h3>
            <p>Excepteur sint occaecat cupidatat non proident sunt.</p>
        </div>
        <div class="col-md-6 w3agile_newsletter_right">
            <form action="#" method="post">
                <input name="Email" placeholder="Email" required="" type="email">
                <input value="" type="submit">
            </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<?php include_once('include/footer.php'); ?>
