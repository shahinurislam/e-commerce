<?php
include_once '../../../vendor/autoload.php';
App\Session::init();

if(empty($_POST['name'])){
    App\Session::flash('error', 'Error !!');
    header('location:create.php');
}else {

    $category = new \App\Category\Category();
    $result = $category->set($_POST)->store();


    if ($result == true || $result == 1) {
        App\Session::flash('success', 'Product  Added Successfully');
        header('location:create.php');
    }
}