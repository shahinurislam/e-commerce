<?php
include_once '../../../vendor/autoload.php';

App\Session::init();
$media = new App\Media\Media();
$media->setSize($_POST);
$nameExits = $media->getSizeByname(false, true);
if($nameExits == 0 ) {
    $result = $media->storeSize();
    if($result == true || $result == 1){
        App\Session::flash('success', 'Image Size Add Successfully');
    }else{
        App\Session::flash('error', 'Image Not Add');

    }
}else{
    App\Session::flash('error', 'Image Size Name Already Exits');
}
header("Location: ".App\Helper::config('config.basePath')."view/admin/media/add_size.php");