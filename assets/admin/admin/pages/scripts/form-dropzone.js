var FormDropzone = function () {


    return {
        //main function to initiate the module
        init: function () {  

            Dropzone.options.myDropzone = {
                paramName: "imageFile",
                init: function() {
                    this.on("addedfile", function(file) {



                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block kkllllpalash'>Remove file</button>");
                        
                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function(e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();
                            var mediaId = $(this).parents(".dz-preview").data("media-id");
                            var ajax_url = $(this).parents("form").data('remove-url');
                            //console.log(mediaId);
                            jQuery.ajax({
                                url: ajax_url,
                                type: 'post',
                                dataType:'json',
                                data: {mediaId: mediaId},
                                success: function (response) {
                                    if(response.status === true){
                                        _this.removeFile(file);
                                    }
                                }
                            });
                            // Remove the file preview.
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });
                    this.on("success", function (file, Response) {
                        var Obj = JSON.parse(Response);
                        if(Obj.status === true) {
                            $(file.previewElement).attr('data-media-id', Obj.mediaId);
                            //console.log(file);
                            //console.log(Obj);
                        }
                    });
                }            
            }
        }
    };
}();