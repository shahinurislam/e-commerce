var UIExtendedModals = function () {

    
    return {
        //main function to initiate the module
        init: function () {
        
            // general settings
            $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
              '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
                '<div class="progress progress-striped active">' +
                  '<div class="progress-bar" style="width: 100%;"></div>' +
                '</div>' +
              '</div>';

            $.fn.modalmanager.defaults.resize = true;

            //dynamic demo:
           /* $('.dynamic .demo').click(function(){
              var tmpl = [
                // tabindex is required for focus
                '<div class="modal hide fade" tabindex="-1">',
                  '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
                    '<h4 class="modal-title">Modal header</h4>', 
                  '</div>',
                  '<div class="modal-body">',
                    '<p>Test</p>',
                  '</div>',
                  '<div class="modal-footer">',
                    '<a href="#" data-dismiss="modal" class="btn btn-default">Close</a>',
                    '<a href="#" class="btn btn-primary">Save changes</a>',
                  '</div>',
                '</div>'
              ].join('');
              
              $(tmpl).modal();
            }); */

            //ajax demo:
            var $modal = $('#media-manager');

            $('#media-open').on('click', function(e){
                e.preventdefault();
                // create the backdrop and wait for next modal to be triggered
                $('body').modalmanager('loading');
/*
                jQuery.ajax({
                    url: 'view/admin/product/ajaxMedia.php',
                    type: 'post',
                    dataType:'json',
                    //data: {mediaId: mediaId},
                    success: function (response) {
                        $modal.modal();
                        if(response.status === true){
                            var mediaHtml = '<div class="media-items">';
                            var mediaData = response.mediaData;
                            mediaData.forEach(function (item, index) {
                                mediaHtml += '<div class="single-item" data-id="'+item.id+'">'
                                    var location = JSON.parse(item.location);
                                    mediaHtml += '<img src="view/'+location.small+'">'
                                mediaHtml += '</div>'
                            });
                            mediaHtml += '</div>';
                            mediaHtml += '</div>';
                            $modal.find('.modal-body').empty();
                            $modal.find('.modal-body').append(mediaHtml);


                        }
                    }
                });
                // setTimeout(function(){
                //   //$modal.load('ajaxMedia.php', '', function(){
                //   $modal.modal();
                // //});
                // }, 1000);*/

            });
            //$modal.on('shown.bs.modal', function (e) {
            // var mediaBody = $modal.find('.modal-body > .media-items');
            // $('.single-item').on('click', function (e) {
            //     $(this).addClass('active').siblings().removeClass('active');
            // });
            //
            // $modal.find('.modal-footer').children('.media-insert')
            //     .live('click', function (e) {
            //         var mediaId = mediaBody.find('.single-item.active').data('id'),
            //             mediaSrc = mediaBody.find('.single-item.active').children('img').attr('src'),
            //         medaiTableHtml = '';
            //         medaiTableHtml += '<tr>';
            //         medaiTableHtml += '<td>';
            //         medaiTableHtml += '<img class="img-responsive" src="'+mediaSrc+'" alt="">';
            //         medaiTableHtml += '<input type="hidden" name="productImgs[]" value="'+mediaId+'">';
            //         medaiTableHtml += '</td>';
            //
            //         medaiTableHtml += '<td>';
            //         medaiTableHtml += 'ssssssssssss';
            //         medaiTableHtml += '</td>';
            //
            //         medaiTableHtml += '<td>';
            //         medaiTableHtml += '<label>';
            //         medaiTableHtml += '<div class="radio"><span>';
            //         medaiTableHtml += '<input name="productImgPrimary" value="'+mediaId+'" type="radio">';
            //         medaiTableHtml += '</div></span>';
            //         medaiTableHtml += '</label>';
            //         medaiTableHtml += '</td>';
            //
            //         medaiTableHtml += '<td>';
            //         medaiTableHtml += '<a class="btn default btn-sm" href=""><i class="fa fa-times"></i> Remove </a>';
            //         medaiTableHtml += '</td>';
            //         medaiTableHtml += '</tr>';
            //         $('.product-images').find('tbody').append(medaiTableHtml);
            //         medaiTableHtml = '';
            //         $modal.modal('hide');
            //     });
            //});
            /*$modal.on('click', '.update', function(){
              $modal.modal('loading');
              setTimeout(function(){
                $modal
                  .modal('loading')
                  .find('.modal-body')
                    .prepend('<div class="alert alert-info fade in">' +
                      'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '</div>');
              }, 1000);
            });*/
        }

    };

}();