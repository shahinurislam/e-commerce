<?php


namespace App\Product;
use App\Database;
use PDO;
use PDOException;
use  App\Session;
use  App\Media\Media;
use App\Category\Category;

class Product extends Database
{
    private $table = 'products';
    private $product_cat = 'product_categorys';
    private $product_image = 'product_images';
     private $name;
     private $description;
     private $short_description;
     private $quantity;
     private $price;
     private $status;
     private $id;
     private $categories;
     private $productImgs;

     public function set($data = array()){
         if(array_key_exists('name',$data)){
             $this->name = $data['name'];
         }if(array_key_exists('description',$data)){
             $this->description = $data['description'];
         }if(array_key_exists('short_description',$data)){
             $this->short_description = $data['short_description'];
         }
         if(array_key_exists('quantity',$data)){
             $this->quantity = $data['quantity'];
         }
         if(array_key_exists('price',$data)){
             $this->price = $data['price'];
         }
         if(array_key_exists('status',$data)){
             $this->status = $data['status'];
         }if(array_key_exists('id',$data)){
             $this->id = $data['id'];
         }
         if(array_key_exists('categories',$data)){
            $this->categories = $data['categories'];
         }

         if(array_key_exists('productImgs',$data)){
            $this->productImgs = $data['productImgs'];
         }
         return $this;
     }

    public function store(){
     try {

         $stm =  $this->con->prepare("INSERT INTO `shoping`.`products` (`id`, `user_id`, `code`, `name`, `description`, `short_description`, `quantity`, `price`, `status`, `created_it`, `updated_it`) VALUES (NULL, '', '', :name, :description, :short_description, :quantity, :price, :status, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)");

         $result =$stm->execute(array(
             ':name' => $this->name,
             ':description' => $this->description,
             ':short_description' => $this->short_description,
             ':quantity' => $this->quantity,
             ':price' => $this->price,
             ':status' => $this->status


         ));
         $currentId = $this->con->lastInsertId();
         $this->productCatInsert($currentId , $this->categories);
         $this->productImagesInsert($currentId , $this->productImgs);
         return $result;
     } catch (PDOException $e) {
         print "Error!: " . $e->getMessage() . "<br/>";
         die();
     }
 }

    public function productCatInsert($id , $cats){
        $catid = '';
        $stm =  $this->con->prepare("INSERT INTO {$this->product_cat} ( product_id, category_id) VALUES (:pid, :catid)");
        $stm->bindParam(':pid', $id, PDO::PARAM_INT);
        $stm->bindParam(':catid', $catid, PDO::PARAM_INT);
        foreach ($cats as $catid){
            $catid = $catid;
            $result = $stm->execute();
        }

    }

    public function productCatDelete($id){
        if (!empty($id)) {
            $stm = $this->con->prepare("DELETE FROM {$this->product_cat} WHERE product_id =:id");
            $stm->bindParam(':id', $id, PDO::PARAM_INT);
            $result = $stm->execute();
        }

    }

    public function productImagesInsert($id , $imagesId){
        $imgid = '';
        $stm =  $this->con->prepare("INSERT INTO {$this->product_image} ( product_id, media_id) VALUES (:pid, :imgid)");
        $stm->bindParam(':pid', $id, PDO::PARAM_INT);
        $stm->bindParam(':imgid', $imgid, PDO::PARAM_INT);
        foreach ($imagesId as $imgid){
            $imgid = $imgid;
            $result = $stm->execute();
        }

    }

    public function productImagesDelete($id){
        if(!empty($id)) {
            $stm = $this->con->prepare("DELETE FROM {$this->product_image} WHERE product_id =:id");
            $stm->bindParam(':id', $id, PDO::PARAM_INT);
            $result = $stm->execute();
        }
    }

    public function getProductCatIdsById($id){
        if (!empty($id)) {

            $stm = $this->con->prepare("SELECT  GROUP_CONCAT(category_id) AS cates FROM {$this->product_cat} WHERE product_id=:product_id");

            $stm->bindParam(':product_id', $id, PDO::PARAM_INT);
            $stm->execute();
            $result = $stm->fetchObject();
            return explode(',', $result->cates);
        }

    }

    public function getProductImagesById($id){
        if (!empty($id)) {
            $mediaObj = new Media();
            $mediaTbl = $mediaObj->getTable();

            $stm = $this->con->prepare("SELECT {$mediaTbl}.* FROM {$mediaTbl} LEFT JOIN {$this->product_image} ON {$this->product_image}.media_id = {$mediaTbl}.id WHERE {$this->product_image}.product_id=:productid");

            $stm->bindParam(':productid', $id, PDO::PARAM_INT);
            $stm->execute();
            $result = $stm->fetchAll();
            return $result;
        }

    }

    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `products` WHERE `status` = 'Published'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function unpublish(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `products` WHERE `status` = 'Not Published' || `status` = ''");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `products` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location: view.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    //unpulish delete
    public function un_delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `products` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location: unpublish.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `products` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                return $stm->fetch(PDO::FETCH_ASSOC);
            }else{

            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `shoping`.`products` SET `name` = :name, `description` = :description, `short_description` = :short_description, `quantity` = :quantity, `price` = :price, `status` = :status WHERE `products`.`id` = :id");
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':description', $this->description, PDO::PARAM_STR);
            $stmt->bindValue(':short_description', $this->short_description, PDO::PARAM_STR);
            $stmt->bindValue(':quantity', $this->quantity, PDO::PARAM_STR);
            $stmt->bindValue(':price', $this->price, PDO::PARAM_STR);
            $stmt->bindValue(':status', $this->status, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);

            //$stmt->execute();

            $currentId = $this->id;
var_dump($this->productImgs);
            $this->productCatDelete($currentId);
            $this->productImagesDelete($currentId);

            $this->productCatInsert($currentId , $this->categories);
            $this->productImagesInsert($currentId , $this->productImgs);
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:view.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getProductById($id, $returnType = 'array'){
        $catObj     = new Category();
        $mediaObj   = new Media();
        $catTbl = $catObj->getTable();
        $mediaTbl = $mediaObj->getTable();
        try {
//            $query = "SELECT {$this->table}.* FROM {$this->table} ";
//            $query .= "LEFT JOIN {$catTbl} ON {$catTbl}.id= :id";
//            $query .= "WHERE {$this->table}.id= :id";

            $stm =  $this->con->prepare("SELECT GROUP_CONCAT(categorys.id) AS cat_ids, GROUP_CONCAT(categorys.name) AS cat_names, GROUP_CONCAT(media.location SEPARATOR  '; ') AS images, products.* FROM products 
LEFT JOIN product_categorys ON product_categorys.product_id = products.id
LEFT JOIN categorys ON categorys.id = product_categorys.category_id
LEFT JOIN product_images ON product_images.product_id = products.id
LEFT JOIN media ON media.id = product_images.media_id WHERE products.id=:pid");
            $stm->bindValue(':pid', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                return $stm->fetch(PDO::FETCH_ASSOC);
            }else{

            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getAllProducts( $limit = false, $order = false ){
        $catObj     = new Category();
        $mediaObj   = new Media();
        $catTbl = $catObj->getTable();
        $mediaTbl = $mediaObj->getTable();
        try {
            $query = "SELECT products.*, GROUP_CONCAT(categorys.id) AS cat_ids, GROUP_CONCAT(categorys.name) AS cat_names, GROUP_CONCAT(media.location SEPARATOR  '; ') AS images FROM products 
LEFT JOIN product_categorys ON product_categorys.product_id = products.id
LEFT JOIN categorys ON categorys.id = product_categorys.category_id
LEFT JOIN product_images ON product_images.product_id = products.id
LEFT JOIN media ON media.id = product_images.media_id ";
            $query .= "WHERE products.status='Published' ";
            $query .= "GROUP BY products.id ";

            if($order == 'DESC'){
                $query .= "ORDER BY products.id DESC ";
            }

            if($limit != false){
                $query .= "LIMIT {$limit} ";
            }

            $stm =  $this->con->prepare($query);
            $stm->bindValue(':status', 'Published', PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                return $stm->fetchAll();
            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

}