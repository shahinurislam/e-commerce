<?php
namespace App;

class Session{

    public static function init(){
        session_start();
    }

    public static function set($key, $value){
        if(is_array($value)) {
            if (!empty($key) ) {
                $_SESSION[$key] = $value;
                return true;
            }
        }else{
            if (!empty($key) && !empty($value)) {
                $_SESSION[$key] = $value;
                return true;
            }
        }
        return false;
    }

    public static function flash($key, $value){
        if(!empty($key) && !empty($value)){
            $_SESSION['flash']= array();
            $_SESSION['flash'][$key] = $value;
            return true;
        }
        return false;
    }

    public static function keyUnSet($key){
        if(isset($_SESSION[$key])){
            unset($_SESSION[$key]);
        }
    }

    public static function get($key){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }
        return false;
    }

    public static function getflash($key){
        if(isset($_SESSION['flash'])) {
            if (array_key_exists($key, $_SESSION['flash'])) {
                return $_SESSION['flash'][$key];
            }
        }
        return false;
    }

    public static function checkSession(){
        if(self::get('auth') == true){
            return true;
        }else{
            return false;
        }
    }

    public static function destroy(){
        session_destroy();
    }

}