-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2017 at 08:11 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoping`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `parent_id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 0, 'Man', NULL, '2017-10-04 10:08:18', '2017-10-04 10:08:18'),
(2, 0, 'Woman', NULL, '2017-10-04 10:08:18', '2017-10-04 10:08:18'),
(3, 1, 'T-Shart', NULL, '2017-10-04 10:09:21', '2017-10-04 10:09:21'),
(4, 2, 'Three pice', NULL, '2017-10-04 10:09:21', '2017-10-04 10:09:21'),
(6, 3, 'Three', NULL, '2017-10-08 19:27:15', '2017-10-08 19:27:15'),
(7, 6, 'four 1', NULL, '2017-10-08 19:30:25', '2017-10-08 19:30:25'),
(8, 6, 'Four 2', NULL, '2017-10-08 19:30:25', '2017-10-08 19:30:25'),
(9, 1, 'dfgd', NULL, '2017-10-08 19:37:02', '2017-10-08 19:37:02');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `location` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `description`, `location`, `created_at`, `updated_at`) VALUES
(13, 'Ian Sane_The Central Angle_akRlSA', 'Ian Sane_The Central Angle_akRlSA', '{\"default\":\"uploads\\/2017\\\\10\\\\Ian Sane_The Central Angle_akRlSA-201710101245.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\Ian Sane_The Central Angle_akRlSA-201710101245productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\Ian Sane_The Central Angle_akRlSA-201710101245smaill.jpg\"}', '2017-10-10 10:45:52', '2017-10-10 10:45:52'),
(14, 'reddreams85_Beautiful JEJU island waterfall_akxjQg', 'reddreams85_Beautiful JEJU island waterfall_akxjQg', '{\"default\":\"uploads\\/2017\\\\10\\\\reddreams85_Beautiful JEJU island waterfall_akxjQg-201710101257.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\reddreams85_Beautiful JEJU island waterfall_akxjQg-201710101257productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\reddreams85_Beautiful JEJU island waterfall_akxjQg-201710101257smaill.jpg\"}', '2017-10-10 10:57:59', '2017-10-10 10:57:59'),
(15, 'Bill Tanata_Prairie Sunset_ZU1qRg', 'Bill Tanata_Prairie Sunset_ZU1qRg', '{\"default\":\"uploads\\/2017\\\\10\\\\Bill Tanata_Prairie Sunset_ZU1qRg-201710101258.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\Bill Tanata_Prairie Sunset_ZU1qRg-201710101258productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\Bill Tanata_Prairie Sunset_ZU1qRg-201710101258smaill.jpg\"}', '2017-10-10 10:58:08', '2017-10-10 10:58:08'),
(16, 'Alias 0591_Autumn_ZEdqQA', 'Alias 0591_Autumn_ZEdqQA', '{\"default\":\"uploads\\/2017\\\\10\\\\Alias 0591_Autumn_ZEdqQA-201710101258.jpg\",\"productImg\":\"uploads\\/2017\\\\10\\\\Alias 0591_Autumn_ZEdqQA-201710101258productImg.jpg\",\"small\":\"uploads\\/2017\\\\10\\\\Alias 0591_Autumn_ZEdqQA-201710101258smaill.jpg\"}', '2017-10-10 10:58:17', '2017-10-10 10:58:17');

-- --------------------------------------------------------

--
-- Table structure for table `media_img_size`
--

CREATE TABLE `media_img_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_img_size`
--

INSERT INTO `media_img_size` (`id`, `name`, `width`, `height`) VALUES
(1, 'palash', 150, 222),
(2, 'dfdfg', 654, 4545),
(3, 'cvbbvc', 4544, 4865),
(11, 'ghfghf', 45645, 46546),
(12, 'jkl', 45645, 4564),
(13, 'gfhfgh', 45555564, 645654),
(14, 'cvbbvcfgfghghf', 414, 4564),
(15, 'ghfhg', 4454, 46564),
(16, 'fghfgghf', 445, 46546);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_description` mediumtext NOT NULL,
  `description` mediumtext,
  `price` decimal(10,0) NOT NULL,
  `status` enum('Published','Not Published') NOT NULL,
  `created_it` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_it` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `code`, `name`, `short_description`, `description`, `price`, `status`, `created_it`, `updated_it`, `quantity`) VALUES
(19, 0, '', 'T shirt ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nWhy do we use it?\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n', '599', 'Published', '2017-10-10 10:56:38', '2017-10-10 10:56:38', 66),
(20, 0, '', 'Shire', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '\r\nWhat is Lorem Ipsum?\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\nWhy do we use it?\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n', '1500', 'Published', '2017-10-10 10:57:38', '2017-10-10 10:57:38', 200);

-- --------------------------------------------------------

--
-- Table structure for table `product_categorys`
--

CREATE TABLE `product_categorys` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_categorys`
--

INSERT INTO `product_categorys` (`id`, `product_id`, `category_id`) VALUES
(52, 18, 1),
(53, 18, 2),
(57, 19, 1),
(58, 19, 3),
(59, 20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `media_id`) VALUES
(32, 18, 8),
(33, 18, 9),
(35, 19, 13),
(36, 19, 16),
(37, 20, 14),
(38, 20, 15);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `rule` enum('Admin','Editor','Customer','') NOT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `user_status` enum('-1','0','1','') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `rule`, `activation_key`, `user_status`, `created_at`, `updated_at`) VALUES
(11, 'admin', 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Customer', '3199', '1', '2017-10-04 10:01:39', '2017-10-04 10:01:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_img_size`
--
ALTER TABLE `media_img_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categorys`
--
ALTER TABLE `product_categorys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `media_img_size`
--
ALTER TABLE `media_img_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `product_categorys`
--
ALTER TABLE `product_categorys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
